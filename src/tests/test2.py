from educelab import mets
import pkg_resources

# random mets files are provided - 1-10
xml_file = pkg_resources.resource_filename(__name__, '/examples/mets1.xml')

x = mets.METSDocument.from_file(xml_file)
y = x.generate_string()
print(y)
