from educelab import mets
from lxml import etree

agent = mets.Agent(mets.Name('James Bond'), 'CREATOR')
header = mets.Header(agents=[agent], element_id='header_id')

xml = etree.fromstring('<xml>some data</xml>')
xml_data = mets.XMLData(xml)
mdwrap = mets.MetadataWrapper('OTHER', xml_data, element_id='wrapper_id', size=4567)
dmdsec = mets.DescriptiveMetadataSection('DMD_ID', admid='ADM_ID', mdwrap=mdwrap)

mdref = mets.MetadataReference('URL', 'MODS', 'URL LINK HERE', label='MDREF LABEL')
rights = mets.PropertyRightsMetadata('rights_id', mdref=mdref)
amdsec = mets.AdministrativeMetadataSection('amd_ID', rights_md=rights)

mechanism = mets.ExecutableMechanism('URL', 'URI LINK', label='IMAGE ANALYSIS')
behavior = mets.Behavior(mechanism, element_id='behavior1')
behsec = mets.BehaviorSection('secID', behaviors=behavior)

location = mets.FileLocation('DOI', 'DOI CODE')
stream = mets.Stream(owner_id='James Bond', stream_type='AUDIO/X WAV')
file = mets.File('file1', checksum='150053', checksum_type='MD5', file_locations=location, streams=stream)
file_group = mets.FileGroup(file, use='free to use')
file_sec = mets.FileSection(file_group, other_attribs=[['random', 'attribute']])

div = mets.Division(element_id='my_div')
struct_map = mets.StructuralMap(div, element_id='my_id')

doc = mets.METSDocument(struct_map, header=header, dmd=dmdsec, amd=amdsec, behavior=behsec, file_sec=file_sec,
                        label='METS DOCUMENT LABEL')

x = doc.generate_tree()
y = x.generate_string()
print(y)
