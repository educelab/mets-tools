import argparse
import re
from enum import Enum

import lxml.etree as etree


class ExitCode(Enum):
    Success = 0
    Warning = 1
    Error = 2


def main():
    """
    Validate an XML file against a given schema
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--input', '-i', required=True, type=str, help='Input XML file')
    parser.add_argument('--schema', '-s', required=True, type=str, help='Schema XSD file')
    args = parser.parse_args()

    # Final values
    exit_code = ExitCode.Success
    error_count = 0

    # Load the XML
    xml_parser = etree.XMLParser(recover=True)
    try:
        xml = etree.parse(source=args.input, parser=xml_parser)
        errs = len(xml_parser.error_log)
        error_count += errs
        if errs > 0:
            print(f'Found {errs} parsing errors and warnings:')
            for e in xml_parser.error_log:
                msg = str(e)
                msg = re.sub(r'^.*?:(?P<line>\d+):(?P<col>\d+):(?P<type>ERROR|WARNING):[A-Z_]+:[A-Z_]+:\s',
                             r'Line \g<line>: \g<type>: ', msg)
                print(msg)
            print()
            exit_code = ExitCode.Warning
    except etree.XMLSyntaxError as e:
        print(f'Irrecoverable XML syntax error: {e}')
        exit(ExitCode.Error.value)

    # Load the schema
    schema = etree.XMLSchema(file=args.schema)

    # Validate the XML
    if not schema.validate(xml):
        errs = len(schema.error_log)
        error_count += errs

        print(f'Found {errs} validation errors and warnings:')
        for e in schema.error_log:
            msg = str(e)
            msg = re.sub(r'^.*?:(?P<line>\d+):(?P<col>\d+):(?P<type>ERROR|WARNING):[A-Z_]+:[A-Z_]+:\s',
                         r'Line \g<line>: \g<type>: ', msg)
            print(msg)
        print()
        exit_code = ExitCode.Warning

    if error_count > 0:
        print(f'Total errors and warnings: {error_count}')
        print(f'File does not validate: {args.input}')
    else:
        print(f'File validates: {args.input}')

    exit(exit_code.value)


if __name__ == '__main__':
    main()
