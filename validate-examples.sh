#!/bin/bash

max_exit=0
for xml in examples/*.xml; do
  echo "----- Validating ${xml} -----"
  python tools/validate_xml.py -i ${xml} -s ${SCHEMA} || true
  exit_code=$?
  if [ $exit_code -gt $max_exit ]; then
    max_exit=$exit_code
  fi
  echo
done
exit $max_exit
